from __future__ import unicode_literals

from django.db import models
from froala_editor.fields import FroalaField

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length= 255)
    text = FroalaField()

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'site'
        verbose_name = ('Site')
        verbose_name_plural = ('Sites')