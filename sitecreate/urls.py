# coding: utf8

from django.conf.urls import url
from . import views
app_name = 'sitecreate'

urlpatterns = [
    url(r'^$', views.index , name = 'index_site'),
]
