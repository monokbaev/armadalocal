from django.shortcuts import render
from django.template.context_processors import csrf

from .models import *
from armada.models import Contact, Address, FooterInfo, GlobalMeta
# Create your views here.
def index(request, context = {}):
    context.update(csrf(request))
    try:
        article = Article.objects.get()
    except Article.DoesNotExist:
        article = False
    context ['article'] = article
    try:
        finfo = FooterInfo.objects.get()
    except FooterInfo.DoesNotExist:
        finfo = False
    context ['finfo'] = finfo
    try:
        address = Address.objects.all()
    except Address.DoesNotExist:
        address = False
    context['addresses'] = address
    try:
        contact = Contact.objects.all()
    except Contact.DoesNotExist:
        contact = False
    context['contacts'] = contact
    context['lmeta'] = GlobalMeta.objects.filter(title="Site")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")

    return render(request, 'site/index.html', context)