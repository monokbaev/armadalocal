# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-08 17:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20160308_2300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blog',
            name='lines',
        ),
    ]
