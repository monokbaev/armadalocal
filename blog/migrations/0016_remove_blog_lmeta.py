# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-09 08:53
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0015_auto_20160309_1445'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blog',
            name='lmeta',
        ),
    ]
