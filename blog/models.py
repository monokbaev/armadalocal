# coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from froala_editor.fields import FroalaField

from django.conf import settings


# Create your models here.
class Blog(models.Model):
    title = models.CharField(max_length = 100, verbose_name = _('Title'),)
    slug = models.SlugField(max_length = 55, verbose_name = _('Slug'), unique=True)
    image = models.ImageField(upload_to = 'uploads/blog')
    text = FroalaField(verbose_name = _('Text'))
    shown = models.IntegerField(default = 0, verbose_name = _('Views'), editable = False)
    created = models.DateTimeField(auto_now_add = True, verbose_name = _('Publication date'))
    #local=models.ForeignKey(LocalMeta, on_delete= models.CASCADE)

    class Meta:
        db_table = 'blog'
        verbose_name = _('Blog')
        verbose_name_plural = _('Blogs')

    def __unicode__(self):
        return self.title

