# coding: utf8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models

# Create your models here.
class Client(models.Model):
    name = models.CharField(verbose_name = _('Title'), max_length = 255)
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/client')
    url = models.CharField(verbose_name = _('Url'), max_length = 255)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'clients'
        verbose_name = _('Client')
        verbose_name_plural = _('Clients')
