-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2016 at 06:04 AM
-- Server version: 5.5.47-0+deb7u1
-- PHP Version: 5.5.30-1~dotdeb+7.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `armada`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `slug`, `image`, `text`) VALUES
(1, 'О нас', 'o-nas', 'uploads/armada/about/team_rTpaizs.jpg', '<p><span>Рекламно-производственная компания &laquo;АРМАДА&raquo; &ndash; динамично развивающаяся производственная компания в сфере рекламы, действующая на рынке Кыргызстана с 2007года. &bull; Первое (назовем его &laquo;Производственное&raquo;) &mdash; Производство средств наружной рекламы и информации.</span></p>'),
(2, 'Почему мы?', 'pochemu-my', 'uploads/armada/about/logo-2_sK7tvQS.png', '<p><span>Хотите, чтобы Ваша реклама была результативной? Чтобы каждый вложенный в нее сом приводил новых клиентов и приносил прибыль? Вы хотите гарантий успеха? Этот путь начинается с выбора достойного партнера, который справится с зада-чей и обеспечит Вашей рекламной продукции высокий уровень исполнения. Выбирая нас в качестве партнера, Вы принимаете верное решение.</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `title`, `address`) VALUES
(2, 'Физический адрес', 'Б. Баатыра23а/Медерова.'),
(3, 'Email', 'armada.kg@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `armada_footerinfo`
--

CREATE TABLE `armada_footerinfo` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL,
  `created` varchar(250) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `armada_footerinfo`
--

INSERT INTO `armada_footerinfo` (`id`, `image`, `text`, `created`, `created_by`, `logo`, `title`) VALUES
(1, 'uploads/armada/footer/logo-2_EsnpTG9.png', '<p><strong>Рекламно-производственная компания &laquo;АРМАДА&raquo;</strong><span><span>&nbsp;</span>&ndash; динамично развивающаяся производственная компания в сфере рекламы, действующая на рынке Кыргызстана с 2007года.</span></p>', '2016', 'Разработан', 'uploads/armada/goog_logo/goog_tkfJKcj.png', 'Название');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add About', 7, 'add_about'),
(20, 'Can change About', 7, 'change_about'),
(21, 'Can delete About', 7, 'delete_about'),
(22, 'Can add Social', 8, 'add_social'),
(23, 'Can change Social', 8, 'change_social'),
(24, 'Can delete Social', 8, 'delete_social'),
(25, 'Can add Contact', 9, 'add_contact'),
(26, 'Can change Contact', 9, 'change_contact'),
(27, 'Can delete Contact', 9, 'delete_contact'),
(28, 'Can add Address', 10, 'add_address'),
(29, 'Can change Address', 10, 'change_address'),
(30, 'Can delete Address', 10, 'delete_address'),
(31, 'Can add Review', 11, 'add_review'),
(32, 'Can change Review', 11, 'change_review'),
(33, 'Can delete Review', 11, 'delete_review'),
(34, 'Can add Offer', 12, 'add_offer'),
(35, 'Can change Offer', 12, 'change_offer'),
(36, 'Can delete Offer', 12, 'delete_offer'),
(37, 'Can add footer info', 13, 'add_footerinfo'),
(38, 'Can change footer info', 13, 'change_footerinfo'),
(39, 'Can delete footer info', 13, 'delete_footerinfo'),
(40, 'Can add Meta', 14, 'add_globalmeta'),
(41, 'Can change Meta', 14, 'change_globalmeta'),
(42, 'Can delete Meta', 14, 'delete_globalmeta'),
(43, 'Can add lMeta', 15, 'add_localmeta'),
(44, 'Can change lMeta', 15, 'change_localmeta'),
(45, 'Can delete lMeta', 15, 'delete_localmeta'),
(46, 'Can add Client', 16, 'add_client'),
(47, 'Can change Client', 16, 'change_client'),
(48, 'Can delete Client', 16, 'delete_client'),
(49, 'Can add News', 17, 'add_news'),
(50, 'Can change News', 17, 'change_news'),
(51, 'Can delete News', 17, 'delete_news'),
(52, 'Can add Blog', 18, 'add_blog'),
(53, 'Can change Blog', 18, 'change_blog'),
(54, 'Can delete Blog', 18, 'delete_blog'),
(55, 'Can add Slider', 19, 'add_slider'),
(56, 'Can change Slider', 19, 'change_slider'),
(57, 'Can delete Slider', 19, 'delete_slider'),
(58, 'Can add Call', 20, 'add_call'),
(59, 'Can change Call', 20, 'change_call'),
(60, 'Can delete Call', 20, 'delete_call'),
(61, 'Can add Feedback', 21, 'add_feedback'),
(62, 'Can change Feedback', 21, 'change_feedback'),
(63, 'Can delete Feedback', 21, 'delete_feedback'),
(64, 'Can add About', 22, 'add_about'),
(65, 'Can change About', 22, 'change_about'),
(66, 'Can delete About', 22, 'delete_about'),
(67, 'Can add Category', 23, 'add_category'),
(68, 'Can change Category', 23, 'change_category'),
(69, 'Can delete Category', 23, 'delete_category'),
(70, 'Can add Image', 24, 'add_image'),
(71, 'Can change Image', 24, 'change_image'),
(72, 'Can delete Image', 24, 'delete_image'),
(73, 'Can add Portfolio', 25, 'add_portfolio'),
(74, 'Can change Portfolio', 25, 'change_portfolio'),
(75, 'Can delete Portfolio', 25, 'delete_portfolio'),
(76, 'Can add Image', 26, 'add_image'),
(77, 'Can change Image', 26, 'change_image'),
(78, 'Can delete Image', 26, 'delete_image'),
(79, 'Can add Law', 27, 'add_article'),
(80, 'Can change Law', 27, 'change_article'),
(81, 'Can delete Law', 27, 'delete_article'),
(82, 'Can add Site', 28, 'add_article'),
(83, 'Can change Site', 28, 'change_article'),
(84, 'Can delete Site', 28, 'delete_article'),
(85, 'Can add Shop', 29, 'add_article'),
(86, 'Can change Shop', 29, 'change_article'),
(87, 'Can delete Shop', 29, 'delete_article');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$24000$DcNokQMwKmVe$AQKmD6/6me5t6wPORiiy1JOSGy1vJRUw2+/CXwDA2Cc=', '2016-03-31 13:25:25', 1, 'admin', '', '', '', 1, 1, '2016-03-14 12:24:11');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `text` longtext NOT NULL,
  `shown` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `slug`, `text`, `shown`, `created`, `image`) VALUES
(1, 'Кто мы?', 'kto-my', '<p><span>Рекламно-производственная компания &laquo;АРМАДА&raquo; &ndash; динамично развивающаяся производственная компания, действующая на рынке Кыргызстана с 2007года. &bull; Первое (назовем его &laquo;Производственное&raquo;) &mdash; Производство средств наружной, рекламы и информации, полиграфии (классическая типография и типография по производству пластиковых карт), обслуживание клиентов среднего и крупного бизнеса, в основном представителей сетевых компаний всех направлений, представителей строительства и владельцев банков, а также изготовление широкого ассортимента рекламно-сувенирной продукции, оперативное и качественное нанесение логотипов, а также информации о фирме, на любом сувенирном носителе в условиях собственного производства. Благодаря команде высококвалифицированных менеджеров и наличию современного профессионального оборудования, мы обеспечиваем стабильно качественное обслуживание &mdash; это прежде всего высокое качество продукции и оперативные сроки сдачи заказов. Это направление приносит Компании стабильный доход, позволяющий содержать штат (более 15 чел.) сотрудников, приобретать бесценный опыт, быть &laquo;в материале&raquo; того, что востребовано сегодняшним бизнесом, совершенствовать рекламные технологии. &bull; Второе (&laquo;Классическое рекламное&raquo;) &ndash; создание ряда креативных проектов для небольшого числа крупных корпоративных клиентов, потребности которых ставят задачи создания и разработки концепций, образов, идей, PR-мероприятий. Удачный тандем этих направлений позволяет держать низкий уровень цен на производственные расходы (полиграфия, наружные поверхности, пластиковые карты, сувенирная продукция) и на размещение рекламы в СМИ, на щитовых наружных носителях, на производство дизайн &mdash; проектов. Принципы РПК &laquo;АРМАДА&raquo; просты: профессионализм, партнерство, развитие. Клиенты РПК &laquo;АРМАДА&raquo; ценят: демократичность подхода, четкость и динамику процесса, дружелюбие менеджмента. Результаты работы профессионального коллектива РПК &laquo;АРМАДА&raquo; &ndash; это рост клиентских компаний, постоянство в сотрудничестве, яркие творческие находки.</span></p>', 0, '2016-03-14 12:39:29', 'uploads/blog/team.jpg'),
(2, 'Почему мы?', 'pochemu-my', '<p>АРМАДА &ndash; ЭТО ТЕРРИТОРИЯ ВАШЕГО УСПЕХА.</p><p>Хотите, чтобы Ваша реклама была результативной? Чтобы каждый вложенный в нее сом приводил новых клиентов и приносил прибыль? Вы хотите гарантий успеха?</p><p>Этот путь начинается с выбора достойного партнера, который справится с зада-чей и обеспечит Вашей рекламной продукции высокий уровень исполнения.</p><p>Выбирая нас в качестве партнера, Вы принимаете верное решение. Мы профес-сионалы. Многолетний опыт и работа с известными брендами являются 100% га-рантией успешного партнерства. Предлагаем уникальные дизайнерские решения и креативное исполнение поставленной задачи.</p><p>Мы работаем для Вас уже более 6 лет. Мы сделали этот сайт, чтобы Вы могли видеть наши результаты. Чтобы Вы были на 100% уверены в своем выборе.</p><p>Мы предлагаем Вам воспользоваться лучшим, что у нас есть. Это доброе имя, че-стные намерения, интеллектуальный подход, приемлемая цена и уникальные возможности в сочетании с большой производственной мощностью. А главное, что нами движет &ndash; это любовь к своему делу. Любой выполняемый нами проект &ndash; плод кро-потливой работы, охватывающий все стороны и грани поставленной задачи. Каж-дый наш шаг от анализа технического задания и разработки идеи до проектирования и производства рекламных материалов выверен до мелочей. Ра-ботая с нами, Вы сами убедитесь в этом.</p><p>АРМАДА - НАША СИЛА В ВАШЕМ УСПЕХЕ! &nbsp;</p>', 0, '2016-03-14 12:40:06', 'uploads/blog/team_cIuMfq8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `call`
--

CREATE TABLE `call` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `service` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `call`
--

INSERT INTO `call` (`id`, `name`, `phone`, `service`) VALUES
(1, 'Demarko', '152608', ''),
(2, 'Demarko', '152608', ''),
(3, 'Demarko', '152608', ''),
(4, 'Demarko', '152608', ''),
(5, 'Demarko', '152608', ''),
(6, 'сериал', '0 312 90 81 82', ''),
(7, 'сериал', '152608', ''),
(8, 'Demarko', '0 312 90 81 82', 'None'),
(9, 'Demarko', '0 312 90 81 82', 'None'),
(10, 'Demarko', '0 312 90 81 82', 'None'),
(11, 'Demarko', '0 312 90 81 82', 'None'),
(12, 'Demarko', '0 312 90 81 82', 'None'),
(13, 'Demarko', '0 312 90 81 82', 'None'),
(14, 'Demarko', '0 312 90 81 82', 'None'),
(15, 'Demarko', '0 312 90 81 82', 'Объемные буквы'),
(16, 'Demarko', '0 312 90 81 82', 'Объемные буквы'),
(17, 'Demarko', '0 312 90 81 82', 'Услуга не выбрана'),
(18, 'Demarko', '0 312 90 81 82', 'Услуга не выбрана'),
(19, 'сериал', '0 312 90 81 82', 'Объемные буквы'),
(20, 'Demarko', '0 312 90 81 82', ''),
(21, 'Demarko', '0 312 90 81 82', ''),
(22, 'Demarko', '0 312 90 81 82', 'Услуга не выбрана');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `image`, `url`) VALUES
(1, 'Лекарь', 'uploads/client/1_7GCQ8vp.png', 'http://lekar.kg/http://lekar.kg/'),
(2, 'International', 'uploads/client/5_bFv78jK.png', '1'),
(3, 'Sah Travel', 'uploads/client/4_du4oMqh.png', '#'),
(4, 'Советская столовая', 'uploads/client/2_9gNDHN4.png', '#'),
(5, '40x40', 'uploads/client/12_dr47k2b.png', '#'),
(6, 'Demarko', 'uploads/client/11_L6jvxbM.png', '#'),
(7, 'Оптима', 'uploads/client/6_kUhjmIG.png', '#');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `image`, `phone`) VALUES
(1, 'Билайн', 'uploads/armada/contact/beeline_jOVHCHj.png', '0771 360 905'),
(2, 'Ошка', 'uploads/armada/contact/o_3JPNGrA.png', '0700 477 594 0703 756 525'),
(3, 'Мегаком', 'uploads/armada/contact/megafon_RTUFbZ2.png', '0553 577 575'),
(4, 'Рабочий', 'uploads/armada/contact/phone_CBtp2ti.png', '0312 475 107, 0312 477 594');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2016-03-14 12:29:10', '1', 'О нас', 1, 'Добавлено.', 7, 1),
(2, '2016-03-14 12:29:44', '2', 'Почему мы?', 1, 'Добавлено.', 7, 1),
(3, '2016-03-14 12:30:17', '1', '1', 1, 'Добавлено.', 10, 1),
(4, '2016-03-14 12:31:18', '2', 'Физический адрес', 1, 'Добавлено.', 10, 1),
(5, '2016-03-14 12:31:24', '1', '1', 3, '', 10, 1),
(6, '2016-03-14 12:31:54', '3', 'Email', 1, 'Добавлено.', 10, 1),
(7, '2016-03-14 12:32:48', '1', 'Билайн', 1, 'Добавлено.', 9, 1),
(8, '2016-03-14 12:32:50', '1', 'Билайн', 2, 'Ни одно поле не изменено.', 9, 1),
(9, '2016-03-14 12:33:16', '2', 'Ошка', 1, 'Добавлено.', 9, 1),
(10, '2016-03-14 12:33:38', '3', 'Мегаком', 1, 'Добавлено.', 9, 1),
(11, '2016-03-14 12:33:59', '4', 'Рабочий', 1, 'Добавлено.', 9, 1),
(12, '2016-03-14 12:35:13', '1', 'Название', 1, 'Добавлено.', 13, 1),
(13, '2016-03-14 12:36:00', '1', 'Качествo', 1, 'Добавлено.', 12, 1),
(14, '2016-03-14 12:36:22', '2', 'Опыт в сфере рекламных услуг', 1, 'Добавлено.', 12, 1),
(15, '2016-03-14 12:36:42', '3', 'Гибкость', 1, 'Добавлено.', 12, 1),
(16, '2016-03-14 12:37:01', '4', 'Собственное производство', 1, 'Добавлено.', 12, 1),
(17, '2016-03-14 12:37:30', '1', 'Надежда Викторовна', 1, 'Добавлено.', 11, 1),
(18, '2016-03-14 12:37:49', '2', 'Сергей Ковалюк', 1, 'Добавлено.', 11, 1),
(19, '2016-03-14 12:38:23', '1', 'Facebook', 1, 'Добавлено.', 8, 1),
(20, '2016-03-14 12:39:29', '1', 'Кто мы?', 1, 'Добавлено.', 18, 1),
(21, '2016-03-14 12:40:06', '2', 'Почему мы?', 1, 'Добавлено.', 18, 1),
(22, '2016-03-14 12:41:24', '1', 'Лекарь', 1, 'Добавлено.', 16, 1),
(23, '2016-03-14 12:41:46', '2', 'International', 1, 'Добавлено.', 16, 1),
(24, '2016-03-14 12:42:11', '3', 'Sah Travel', 1, 'Добавлено.', 16, 1),
(25, '2016-03-14 12:42:34', '4', 'Советская столовая', 1, 'Добавлено.', 16, 1),
(26, '2016-03-14 12:42:56', '5', '40x40', 1, 'Добавлено.', 16, 1),
(27, '2016-03-14 12:43:14', '6', 'Demarko', 1, 'Добавлено.', 16, 1),
(28, '2016-03-14 12:43:33', '7', 'Оптима', 1, 'Добавлено.', 16, 1),
(29, '2016-03-14 12:44:46', '1', 'Название закона', 1, 'Добавлено.', 27, 1),
(30, '2016-03-14 12:46:55', '1', 'Создание сайтов в рассрочку', 1, 'Добавлено.', 17, 1),
(31, '2016-03-14 12:47:28', '2', 'Создание сайтов и разработка', 1, 'Добавлено.', 17, 1),
(32, '2016-03-14 12:48:17', '3', 'Наружная реклама в Бишкеке или Интернет реклама', 1, 'Добавлено.', 17, 1),
(33, '2016-03-14 12:49:23', '1', 'Дизайн интерьера', 1, 'Добавлено.', 19, 1),
(34, '2016-03-14 12:52:36', '1', 'Дизайн интерьера', 2, 'Изменен icon.', 19, 1),
(35, '2016-03-14 12:53:21', '2', 'Название закона', 1, 'Добавлено.', 19, 1),
(36, '2016-03-14 12:54:14', '2', 'Название закона', 2, 'Изменен text.', 19, 1),
(37, '2016-03-15 10:25:42', '2', 'Почему мы?', 2, 'Ни одно поле не изменено.', 18, 1),
(38, '2016-03-23 09:49:04', '1', 'Global', 1, 'Добавлено.', 14, 1),
(39, '2016-03-23 09:51:56', '1', 'Lhead', 1, 'Добавлено.', 15, 1),
(40, '2016-03-23 09:52:51', '2', 'Создание сайтов в рассрочку', 1, 'Добавлено.', 15, 1),
(41, '2016-03-23 10:00:20', '2', 'News', 1, 'Добавлено.', 14, 1),
(42, '2016-03-26 06:45:21', '1', 'Наружная реклама', 1, 'Добавлено.', 22, 1),
(43, '2016-03-26 06:46:20', '1', 'Объемные буквы', 1, 'Добавлено.', 23, 1),
(44, '2016-03-26 06:47:47', '1', 'Объемные буквы', 2, 'Добавлен Изображение "Лекарь". Добавлен Изображение "авыаыа". Добавлен Изображение "sadsad".', 23, 1),
(45, '2016-03-28 10:18:13', '1', 'Баннера', 1, 'Добавлено. Добавлен Изображение "Image object".', 25, 1),
(46, '2016-03-28 10:18:43', '2', 'Нарезанные буквы', 1, 'Добавлено. Добавлен Изображение "Image object".', 25, 1),
(47, '2016-03-28 10:23:07', '1', 'Магазин', 1, 'Добавлено.', 29, 1),
(48, '2016-03-30 09:57:49', '1', 'Форекс 3 мм', 2, 'Изменен title и text.', 29, 1),
(49, '2016-03-30 09:59:26', '1', 'Форекс 3 мм', 2, 'Изменен text.', 29, 1),
(50, '2016-03-30 10:00:41', '1', 'Форекс 3 мм', 2, 'Изменен text.', 29, 1),
(51, '2016-03-30 10:02:22', '1', 'Форекс 3 мм', 2, 'Изменен text.', 29, 1),
(52, '2016-03-30 10:04:11', '2', 'Акрил 3 мм более 30 цветов', 1, 'Добавлено.', 29, 1),
(53, '2016-03-30 10:06:59', '1', 'Форекс 3 мм', 2, 'Изменен text.', 29, 1),
(54, '2016-03-30 10:07:09', '2', 'Акрил 3 мм более 30 цветов', 2, 'Изменен text.', 29, 1),
(55, '2016-03-30 10:07:26', '1', 'Форекс 3 мм', 2, 'Изменен text.', 29, 1),
(56, '2016-03-30 10:08:28', '1', 'Создание сайта', 1, 'Добавлено.', 28, 1),
(57, '2016-03-30 10:10:21', '1', 'Создание сайта', 2, 'Изменен text.', 28, 1),
(58, '2016-03-30 10:11:51', '1', 'Новый закон о рекламе', 2, 'Изменен title и text.', 27, 1),
(59, '2016-03-30 10:13:10', '2', 'Кыргызский язык в рекламе', 1, 'Добавлено.', 27, 1),
(60, '2016-03-30 13:31:09', '1', 'Объемные буквы', 2, 'Ни одно поле не изменено.', 23, 1),
(61, '2016-03-30 15:02:16', '2', 'Нарезанные буквы', 2, 'Изменены image для Изображение "Image object".', 25, 1),
(62, '2016-03-30 15:10:47', '2', 'Нарезанные буквы', 3, '', 25, 1),
(63, '2016-03-30 15:10:47', '1', 'Баннера', 3, '', 25, 1),
(64, '2016-03-31 05:24:23', '6', 'Название закона', 1, 'Добавлено. Добавлен Изображение "Image object".', 25, 1),
(65, '2016-03-31 05:27:28', '1', 'Форекс 3 мм', 2, 'Изменен text.', 29, 1),
(66, '2016-03-31 05:27:35', '2', 'Акрил 3 мм более 30 цветов', 3, '', 29, 1),
(67, '2016-03-31 05:28:49', '6', 'Название закона', 2, 'Изменены image для Изображение "Image object".', 25, 1),
(68, '2016-03-31 13:43:48', '7', 'Баннеры', 1, 'Добавлено. Добавлен Изображение "Image object".', 25, 1),
(69, '2016-03-31 13:44:18', '7', 'Баннеры', 2, 'Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(70, '2016-03-31 13:44:36', '7', 'Баннеры', 2, 'Добавлен Изображение "Image object".', 25, 1),
(71, '2016-03-31 13:44:48', '7', 'Баннеры', 2, 'Добавлен Изображение "Image object".', 25, 1),
(72, '2016-03-31 13:44:55', '7', 'Баннеры', 2, 'Добавлен Изображение "Image object".', 25, 1),
(73, '2016-03-31 13:45:02', '7', 'Баннеры', 2, 'Добавлен Изображение "Image object".', 25, 1),
(74, '2016-03-31 13:46:17', '8', 'Заправки', 1, 'Добавлено.', 25, 1),
(75, '2016-03-31 14:02:00', '8', 'Заправки', 2, 'Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(76, '2016-03-31 14:05:10', '9', 'Нарезные буквы', 1, 'Добавлено. Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(77, '2016-03-31 14:19:33', '10', 'Обьемные буквы', 1, 'Добавлено. Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(78, '2016-03-31 14:30:29', '11', 'Светодиодная вывеска', 1, 'Добавлено. Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(79, '2016-03-31 14:32:30', '12', 'Стеллы', 1, 'Добавлено. Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(80, '2016-03-31 14:35:14', '13', 'Таблички и стенды', 1, 'Добавлено. Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(81, '2016-03-31 14:35:54', '14', 'Штендеры', 1, 'Добавлено. Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object". Добавлен Изображение "Image object".', 25, 1),
(82, '2016-03-31 14:38:30', '10', 'Обьемные буквы', 2, 'Удален Изображение "Image object".', 25, 1),
(83, '2016-03-31 14:42:03', '2', 'Название закона', 3, '', 19, 1),
(84, '2016-03-31 14:44:30', '1', 'Дизайн интерьера', 2, 'Изменен image и icon.', 19, 1),
(85, '2016-03-31 14:49:00', '3', 'Форекс 3мм  Цена: 600 с/лист', 1, 'Добавлено.', 29, 1),
(86, '2016-03-31 14:49:44', '1', 'Форекс 3 мм', 3, '', 29, 1),
(87, '2016-03-31 14:56:51', '3', 'Товары', 2, 'Изменен title и text.', 29, 1),
(88, '2016-03-31 14:58:28', '3', 'Товары', 2, 'Изменен text.', 29, 1),
(89, '2016-03-31 14:59:00', '3', 'Товары', 2, 'Ни одно поле не изменено.', 29, 1),
(90, '2016-03-31 14:59:36', '3', 'Товары', 2, 'Изменен text.', 29, 1),
(91, '2016-03-31 15:00:19', '3', 'Товары', 2, 'Изменен text.', 29, 1),
(92, '2016-04-01 06:00:41', '3', 'Товары', 3, '', 29, 1),
(93, '2016-04-01 06:01:31', '4', 'Форекс 3 мм', 1, 'Добавлено.', 29, 1),
(94, '2016-04-01 06:01:45', '5', 'Акрил 3 мм более 30 цветов', 1, 'Добавлено.', 29, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(7, 'armada', 'about'),
(10, 'armada', 'address'),
(9, 'armada', 'contact'),
(13, 'armada', 'footerinfo'),
(14, 'armada', 'globalmeta'),
(15, 'armada', 'localmeta'),
(12, 'armada', 'offer'),
(11, 'armada', 'review'),
(8, 'armada', 'social'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(20, 'backcall', 'call'),
(21, 'backcall', 'feedback'),
(18, 'blog', 'blog'),
(16, 'client', 'client'),
(5, 'contenttypes', 'contenttype'),
(27, 'law', 'article'),
(17, 'news', 'news'),
(26, 'portfolio', 'image'),
(25, 'portfolio', 'portfolio'),
(22, 'services', 'about'),
(23, 'services', 'category'),
(24, 'services', 'image'),
(6, 'sessions', 'session'),
(29, 'shop', 'article'),
(28, 'sitecreate', 'article'),
(19, 'slider', 'slider');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2016-03-14 12:11:48'),
(2, 'auth', '0001_initial', '2016-03-14 12:11:48'),
(3, 'admin', '0001_initial', '2016-03-14 12:11:48'),
(4, 'admin', '0002_logentry_remove_auto_add', '2016-03-14 12:11:48'),
(5, 'news', '0001_initial', '2016-03-14 12:11:48'),
(6, 'news', '0002_news_image', '2016-03-14 12:11:48'),
(7, 'news', '0003_auto_20160308_2322', '2016-03-14 12:11:48'),
(8, 'law', '0001_initial', '2016-03-14 12:11:48'),
(9, 'blog', '0001_initial', '2016-03-14 12:11:49'),
(10, 'blog', '0002_blog_image', '2016-03-14 12:11:49'),
(11, 'blog', '0003_auto_20160308_2247', '2016-03-14 12:11:49'),
(12, 'armada', '0001_initial', '2016-03-14 12:11:49'),
(13, 'armada', '0002_auto_20160116_0754', '2016-03-14 12:11:49'),
(14, 'armada', '0003_auto_20160116_0754', '2016-03-14 12:11:49'),
(15, 'armada', '0004_auto_20160116_0755', '2016-03-14 12:11:49'),
(16, 'armada', '0005_review', '2016-03-14 12:11:49'),
(17, 'armada', '0006_offer', '2016-03-14 12:11:49'),
(18, 'armada', '0007_footerinfo', '2016-03-14 12:11:49'),
(19, 'armada', '0008_auto_20160116_1319', '2016-03-14 12:11:49'),
(20, 'armada', '0009_footerinfo_title', '2016-03-14 12:11:49'),
(21, 'armada', '0010_auto_20160116_1324', '2016-03-14 12:11:49'),
(22, 'armada', '0011_globalmeta', '2016-03-14 12:11:49'),
(23, 'armada', '0012_auto_20160229_2133', '2016-03-14 12:11:49'),
(24, 'armada', '0013_localmeta', '2016-03-14 12:11:49'),
(25, 'blog', '0004_blog_meta', '2016-03-14 12:11:49'),
(26, 'blog', '0005_auto_20160308_2254', '2016-03-14 12:11:49'),
(27, 'blog', '0006_auto_20160308_2257', '2016-03-14 12:11:50'),
(28, 'blog', '0007_auto_20160308_2259', '2016-03-14 12:11:50'),
(29, 'blog', '0008_auto_20160308_2300', '2016-03-14 12:11:50'),
(30, 'blog', '0009_remove_blog_lines', '2016-03-14 12:11:50'),
(31, 'armada', '0014_delete_localmeta', '2016-03-14 12:11:50'),
(32, 'armada', '0015_localmeta', '2016-03-14 12:11:50'),
(33, 'blog', '0010_blog_local', '2016-03-14 12:11:50'),
(34, 'blog', '0011_remove_blog_local', '2016-03-14 12:11:50'),
(35, 'blog', '0012_blog_local', '2016-03-14 12:11:50'),
(36, 'blog', '0013_remove_blog_local', '2016-03-14 12:11:50'),
(37, 'armada', '0016_localmeta_blog', '2016-03-14 12:11:51'),
(38, 'armada', '0017_remove_localmeta_blog', '2016-03-14 12:11:51'),
(39, 'blog', '0014_blog_lmeta', '2016-03-14 12:11:51'),
(40, 'blog', '0015_auto_20160309_1445', '2016-03-14 12:11:51'),
(41, 'blog', '0016_remove_blog_lmeta', '2016-03-14 12:11:51'),
(42, 'armada', '0018_localmeta_belongs_to', '2016-03-14 12:11:51'),
(43, 'armada', '0019_auto_20160309_1511', '2016-03-14 12:11:51'),
(44, 'armada', '0020_auto_20160309_1526', '2016-03-14 12:11:52'),
(45, 'armada', '0021_auto_20160309_1526', '2016-03-14 12:11:52'),
(46, 'armada', '0022_auto_20160309_1537', '2016-03-14 12:11:52'),
(47, 'armada', '0023_remove_localmeta_law', '2016-03-14 12:11:52'),
(48, 'armada', '0024_globalmeta_title', '2016-03-14 12:11:52'),
(49, 'armada', '0025_localmeta_service', '2016-03-14 12:11:52'),
(50, 'contenttypes', '0002_remove_content_type_name', '2016-03-14 12:11:53'),
(51, 'auth', '0002_alter_permission_name_max_length', '2016-03-14 12:11:53'),
(52, 'auth', '0003_alter_user_email_max_length', '2016-03-14 12:11:53'),
(53, 'auth', '0004_alter_user_username_opts', '2016-03-14 12:11:53'),
(54, 'auth', '0005_alter_user_last_login_null', '2016-03-14 12:11:53'),
(55, 'auth', '0006_require_contenttypes_0002', '2016-03-14 12:11:53'),
(56, 'auth', '0007_alter_validators_add_error_messages', '2016-03-14 12:11:53'),
(57, 'backcall', '0001_initial', '2016-03-14 12:11:53'),
(58, 'backcall', '0002_auto_20160307_1835', '2016-03-14 12:11:53'),
(59, 'backcall', '0003_auto_20160307_1836', '2016-03-14 12:11:53'),
(60, 'blog', '0017_auto_20160309_1842', '2016-03-14 12:11:53'),
(61, 'client', '0001_initial', '2016-03-14 12:11:53'),
(62, 'law', '0002_newslocalmeta', '2016-03-14 12:11:53'),
(63, 'law', '0003_delete_newslocalmeta', '2016-03-14 12:11:53'),
(64, 'news', '0004_auto_20160309_1842', '2016-03-14 12:11:53'),
(65, 'portfolio', '0001_initial', '2016-03-14 12:11:53'),
(66, 'portfolio', '0002_auto_20160205_2235', '2016-03-14 12:11:53'),
(67, 'services', '0001_initial', '2016-03-14 12:11:54'),
(68, 'services', '0002_auto_20160130_1801', '2016-03-14 12:11:54'),
(69, 'services', '0003_category_slug', '2016-03-14 12:11:54'),
(70, 'services', '0004_remove_category_title_tdc', '2016-03-14 12:11:54'),
(71, 'services', '0005_about_attach_img', '2016-03-14 12:11:54'),
(72, 'services', '0006_auto_20160205_2235', '2016-03-14 12:11:54'),
(73, 'services', '0007_auto_20160308_2316', '2016-03-14 12:11:54'),
(74, 'services', '0008_auto_20160308_2324', '2016-03-14 12:11:54'),
(75, 'services', '0009_auto_20160309_1401', '2016-03-14 12:11:54'),
(76, 'services', '0010_auto_20160309_1404', '2016-03-14 12:11:55'),
(77, 'services', '0011_auto_20160309_1842', '2016-03-14 12:11:55'),
(78, 'sessions', '0001_initial', '2016-03-14 12:11:55'),
(79, 'shop', '0001_initial', '2016-03-14 12:11:55'),
(80, 'sitecreate', '0001_initial', '2016-03-14 12:11:55'),
(81, 'slider', '0001_initial', '2016-03-14 12:11:55'),
(82, 'armada', '0026_remove_globalmeta_title', '2016-03-23 09:46:28'),
(83, 'armada', '0027_globalmeta_title', '2016-03-23 09:48:25'),
(84, 'armada', '0028_auto_20160323_1557', '2016-03-23 09:57:57'),
(85, 'backcall', '0004_call_service', '2016-03-30 11:00:33'),
(86, 'backcall', '0005_auto_20160401_1159', '2016-04-01 05:59:33'),
(87, 'shop', '0002_auto_20160401_1159', '2016-04-01 05:59:33');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('03dgp6odv09vlhd61fynmv8x0as2w814', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-11 10:16:12'),
('0esoimi5bkdg4fh4a5hv4ynk9tvedac2', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-03-28 12:24:33'),
('424er3tz806c4jo17bhw6s1zii5okjgw', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-06 09:45:05'),
('692i1x16nrj3xhsk0zv1gs053bes8pc1', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-14 05:03:39'),
('a3cszfug01y4paajny53h26ruikl8zs2', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-14 13:25:25'),
('dhe91qoc2ivk9nj7mo9pd2h9h07wft6e', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-09 06:42:38'),
('f9410ofi8zvegogaqv9hovxhv8bqzbka', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-13 13:27:50'),
('il4tjhpwb7pqk451h8z3yd0nljccqfnk', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-03-29 10:25:22'),
('vnqgzgtd78om5z5dvofcsea72zf08mo4', 'MTZhYjZhYWZlYmYwN2FiMzUwNjc1ZjBkNjQ2Mzg0NDdlNjZiMDQyNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZmZDI2ZjNkODY4ODRlMTg4YjBhYWZjYTk4MjIxNzMwNGY3MjIwOTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-04-13 09:52:32');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `text`) VALUES
(1, 'Demarko', 'cdjfdflj@gmail.com', 'цывцфывфывыфв'),
(2, 'Demarko', 'monokbaev@gmail.com', 'ЫФВЫВФЫВФЫВ'),
(3, 'Demarko', 'monokbaev@gmail.com', 'ЫФВЫВФЫВФЫВ'),
(4, 'Demarko', 'chingy89_89@mail.ru', 'цвфцуаВфАЦВАФЦУВ');

-- --------------------------------------------------------

--
-- Table structure for table `Globalmeta`
--

CREATE TABLE `Globalmeta` (
  `id` int(11) NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_desc` longtext NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `title` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Globalmeta`
--

INSERT INTO `Globalmeta` (`id`, `meta_title`, `meta_desc`, `meta_key`, `title`) VALUES
(1, 'Ghead', 'Gdecs', 'Gkey', 'Global'),
(2, 'Newstitle', 'News', 'News', 'News');

-- --------------------------------------------------------

--
-- Table structure for table `law`
--

CREATE TABLE `law` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `law`
--

INSERT INTO `law` (`id`, `title`, `text`) VALUES
(1, 'Новый закон о рекламе', '<p>(В редакции Законов КР от 30 ноября 1999 года N 134, 25 июля 2002 года N 130, 27 января 2006 года N 17, 6 февраля 2006 года N 35, 31 июля 2006 года N 140, 8 августа 2006 года N 159, 17 декабря 2008 года N 264, 29 января 2009 года N 34, 25 апреля 2009 года N 130, 17 июля 2009 года N 228,июля 2012 года N 123, 26 апреля 2013 года N 62<span>)</span></p><p>Глава I. &nbsp; Общие положения<br>Глава II. &nbsp;Общие и специальные требования к рекламе<br>Глава III. Права и обязанности рекламодателей, рекламопроизводителей и рекламораспространителей<br>Глава IV. &nbsp;Государственный контроль и саморегулирование в области рекламы<br>Глава V. &nbsp; Контрреклама и ответственность за ненадлежащую рекламу<br>Глава VI. &nbsp;Заключительные положения</p><p><a rel="nofollow" href="http://www.media.kg/wp-content/uploads/2013/06/%D0%97%D0%B0%D0%BA%D0%BE%D0%BD-%D0%9A%D0%A0-%D0%9E-%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D0%B5-.doc">Скачать полный текст</a> (MS Word 54 KB)</p>'),
(2, 'Кыргызский язык в рекламе', '<p><span><strong>Выписка из Закона &quot;О государственном языке Кыргызской Республики&quot;<br></strong></span><span><strong>Глава 7 - Использование государственного языка в названиях, именах и в информации</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;<strong>Статья 24.</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;Названия Кыргызской &nbsp;Республики, &nbsp; административно-территориальных единиц &nbsp;и &nbsp;объектов &nbsp;(улиц, &nbsp;площадей и т.д.), &nbsp;географические названия оформляются на государственном языке.<br>&nbsp; &nbsp; &nbsp;Наименования государственных и негосударственных предприятий, &nbsp;учреждений, организаций и заведений в обязательном порядке оформляются на государственном и официальном языках.</span></p><p><span>&nbsp; &nbsp; &nbsp;<strong>Статья 25.</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;В Кыргызской Республике запись личных имен и фамилий в документах, удостоверяющих личность, осуществляется на государственном языке с дублированием &nbsp;на официальном языке и соблюдением национальных традиций на основе свободного волеизъявления граждан.<br>&nbsp; &nbsp; &nbsp;Порядок оформления документов, &nbsp;удостоверяющих личность, определяется Правительством Кыргызской Республики.</span></p><p><span>&nbsp; &nbsp; &nbsp;<strong>Статья 26.</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;Официальные печати и бланки органов государственной власти и местного &nbsp;самоуправления, &nbsp;государственных и негосударственных учреждений и организаций выполняются на государственном языке и дублируются на &nbsp;официальном либо иных языках.</span></p><p><span>&nbsp; &nbsp; &nbsp;<strong>Статья 27.</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;В Кыргызской Республике вывески, объявления, прейскуранты и другая наглядная информация оформляются сначала на государственном языке, &nbsp;затем &nbsp;- на официальном языке, &nbsp;а в необходимых случаях также и на других языках.<br>&nbsp; &nbsp; &nbsp;Размер шрифта текста на других языках по величине не должен превышать размера шрифта текста на государственном языке.</span></p><p><span>&nbsp; &nbsp; &nbsp;<strong>Статья 28.</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;Почтовая и телеграфная корреспонденция &nbsp;на &nbsp;территории &nbsp;Кыргызской Республики оформляется на государственном языке, а в необходимых случаях - на официальном языке. &nbsp;Адрес корреспонденции, направляемой за пределы республики, &nbsp;указывается на официальном языке или на соответствующих иностранных языках.</span></p><p><span>&nbsp; &nbsp; &nbsp;<strong>Статья 29.</strong></span></p><p><span>&nbsp; &nbsp; &nbsp;Тексты этикеток товаров, &nbsp;маркировка, &nbsp;номенклатурные списки товаров, &nbsp;инструкции по их использованию, издаваемые в Кыргызской Республике, в обязательном порядке приводятся на государственном языке, а в необходимых случаях - на официальном языке.<br>&nbsp; &nbsp; &nbsp;Специальная информация на иностранном языке, &nbsp;содержащаяся на этикетках, маркировка, инструкции к завезенным из-за рубежа товарам, переводятся на государственный или официальный язык за &nbsp;счет &nbsp;импортирующих фирм.<br>&nbsp; &nbsp; &nbsp;Наименования, утвержденные фирмой при маркировке товарных &nbsp;знаков, сохраняются без перевода на другие языки.</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `Localmeta`
--

CREATE TABLE `Localmeta` (
  `id` int(11) NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_desc` longtext NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Localmeta`
--

INSERT INTO `Localmeta` (`id`, `meta_title`, `meta_desc`, `meta_key`, `blog_id`, `news_id`, `service_id`) VALUES
(1, 'Lhead', 'Ldesc', 'Lkey', 1, NULL, NULL),
(2, 'Создание сайтов в рассрочку', 'Бла', 'Бла', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `text` longtext NOT NULL,
  `shown` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `text`, `shown`, `created`, `image`) VALUES
(1, 'Создание сайтов в рассрочку', 'sozdanie-sajtov-v-rassrochku', '<p><strong><span>Новая услуга - сайт в рассрочку!!!</span></strong><br>Наша компания предлагает создать Вам сайт в рассрочку на взаимовыгодных условиях. Мы доверяем нашим клиентам и даем возможность заплатить за сайт в течении нескольких месяцев.<br>После составления ТЗ Вы вносите определенный % суммы за сайт каждый месяц.</p><p>При минимальных вложениях вы получаете полноценный сайт, реальный инструмент, по-настоящему участвующий в развитии вашего бизнеса.<br>Сайт располагается на нашем хостинге до того пока Вы не внесете полную сумму заказа. После Вы можете перенести его на свой сервер или хостинг.</p>', 0, '2016-03-14 12:46:55', 'uploads/news/images/portfolio_vRXOlMx.jpg'),
(2, 'Создание сайтов и разработка', 'sozdanie-sajtov-i-razrabotka', '<p>Теперь компания Армада разрабатывает сайты любой сложности. Наша профессиональная команда разработчиков имеет большой опыт разработки.<span>&nbsp;</span><strong>Создать и разработать</strong><span>&nbsp;</span>сложный и<strong><span>&nbsp;</span>стильный сайт</strong><span>&nbsp;</span>не составит труда. Принимаем заказы со всего<span>&nbsp;</span><strong>Кыргызстана</strong>. Офис находится<span>&nbsp;</span><strong>в г. Бишкек</strong>, ул. Байтик Баатыра 23а.</p>', 0, '2016-03-14 12:47:28', 'uploads/news/images/team.jpg'),
(3, 'Наружная реклама в Бишкеке или Интернет реклама', 'naruzhnaya-reklama-v-bishkeke-ili-internet-reklama', '<p>Наружная реклама в Бишкеке занимает 18% от всего потока информационной рекламы. В настоящее время очень сильно развивается интернет - реклама которая стремительно растет в Кыргызстане. Колличество kg &nbsp;сайтов уже превышает 5000. Но не смотря на это каждый день в Кыргызстане создается около 10-20 сайтов. В Наружной рекламе преобладает широкоформатная печать, которую вывешивают на городских билбордах. Основной поток клиентов при этом являются горожане которые просматривают эту баннерную рекламу. Люди же которые с утра до вечера работают в офисе, больше склонны просматривать рекламу в сети интернет и на телевидении. Все больше компаний создают свои сайты, все больше компаний начинают свой путь именно с сайта, пуская рекламу в самых посещаемых сайтах Кыргызстана.</p><p>Все же выбирать способ рекламы Вам. Заказать сайт или же <span>напечатать очередной баннер.&nbsp;</span></p>', 0, '2016-03-14 12:48:17', 'uploads/news/images/portfolio_vRXOlMx_v0BCwQn.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `title`, `image`, `text`) VALUES
(1, 'Качествo', 'uploads/armada/offer/4_49qltgk.png', 'Наша компания гарантирует качественный результат получения готового изделия для наших клиентов. Все этапы подготовки и производства осуществляется по отработанной годами схеме реализации идей наших заказчиков.'),
(2, 'Опыт в сфере рекламных услуг', 'uploads/armada/offer/3_SOsP7cc.png', 'Вся продукция, представленная на нашем сайте обусловлена значительным накопленным опытом на рекламном рынке. Нам приходилось решать, как традиционные задачи, так и более сложные проекты.'),
(3, 'Гибкость', 'uploads/armada/offer/2_JZhamVG.png', 'При нашем взаимном сотрудничестве мы ищем индивидуальный подход к каждому заказчику. С самого начала формирования заказа, мы находим оптимальные решения для воплощения самых сложных и нетрадиционных рекламных идей.'),
(4, 'Собственное производство', 'uploads/armada/offer/1_ZD8YZBy.png', 'Компания Армада располагает собственным производством, наличием производственных площадей и собственным оборудованием. Многие рекламные агентства являются посредниками при изготовлении рекламного продукта.');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `title`) VALUES
(6, 'Название закона'),
(7, 'Баннеры'),
(8, 'Заправки'),
(9, 'Нарезные буквы'),
(10, 'Обьемные буквы'),
(11, 'Светодиодная вывеска'),
(12, 'Стеллы'),
(13, 'Таблички и стенды'),
(14, 'Штендеры');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_images`
--

CREATE TABLE `portfolio_images` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio_images`
--

INSERT INTO `portfolio_images` (`id`, `image`, `project_id`) VALUES
(3, 'uploads/portfolio/images/1_Vo3ZLKn.jpg', 6),
(4, 'uploads/portfolio/images/1_800x1065.jpg', 7),
(5, 'uploads/portfolio/images/2_800x601.jpg', 7),
(6, 'uploads/portfolio/images/3_800x1065.jpg', 7),
(7, 'uploads/portfolio/images/5_800x601.jpg', 7),
(8, 'uploads/portfolio/images/6_800x601.jpg', 7),
(9, 'uploads/portfolio/images/7_800x601.jpg', 7),
(10, 'uploads/portfolio/images/8_800x600.jpg', 7),
(11, 'uploads/portfolio/images/1_796x597.jpg', 8),
(12, 'uploads/portfolio/images/2_796x597.jpg', 8),
(13, 'uploads/portfolio/images/4_796x597.jpg', 8),
(14, 'uploads/portfolio/images/4_796x597_jE3hr3k.jpg', 8),
(15, 'uploads/portfolio/images/5_796x597.jpg', 8),
(16, 'uploads/portfolio/images/6_796x597.jpg', 8),
(17, 'uploads/portfolio/images/7_796x597.jpg', 8),
(18, 'uploads/portfolio/images/1_800x600.jpg', 9),
(19, 'uploads/portfolio/images/3_800x1067.jpg', 9),
(20, 'uploads/portfolio/images/7_800x600.jpg', 9),
(21, 'uploads/portfolio/images/8_800x1067.jpg', 9),
(22, 'uploads/portfolio/images/1_800x479.jpg', 10),
(23, 'uploads/portfolio/images/2_800x479.jpg', 10),
(24, 'uploads/portfolio/images/3_800x480.jpg', 10),
(25, 'uploads/portfolio/images/5_800x433.jpg', 10),
(27, 'uploads/portfolio/images/6_800x600.jpg', 10),
(28, 'uploads/portfolio/images/7_800x600_7rpUQi2.jpg', 10),
(29, 'uploads/portfolio/images/8_800x600_6Bp6HAX.jpg', 10),
(30, 'uploads/portfolio/images/9_800x600.jpg', 10),
(31, 'uploads/portfolio/images/10_800x600.jpg', 10),
(32, 'uploads/portfolio/images/11_800x600.jpg', 10),
(33, 'uploads/portfolio/images/12_800x600.jpg', 10),
(34, 'uploads/portfolio/images/14_800x600.jpg', 10),
(35, 'uploads/portfolio/images/15_800x600.jpg', 10),
(36, 'uploads/portfolio/images/16_800x1067.jpg', 10),
(37, 'uploads/portfolio/images/17_800x600.jpg', 10),
(38, 'uploads/portfolio/images/18_800x600.jpg', 10),
(39, 'uploads/portfolio/images/19_800x600.jpg', 10),
(40, 'uploads/portfolio/images/20_800x451.jpg', 10),
(41, 'uploads/portfolio/images/21_800x451.jpg', 10),
(42, 'uploads/portfolio/images/22_800x451.jpg', 10),
(43, 'uploads/portfolio/images/23_800x600.jpg', 10),
(44, 'uploads/portfolio/images/24_800x600.jpg', 10),
(45, 'uploads/portfolio/images/25_800x600.jpg', 10),
(46, 'uploads/portfolio/images/26_800x600.jpg', 10),
(47, 'uploads/portfolio/images/27_800x600.jpg', 10),
(48, 'uploads/portfolio/images/28_800x600.jpg', 10),
(49, 'uploads/portfolio/images/29_800x600.jpg', 10),
(50, 'uploads/portfolio/images/30_800x600.jpg', 10),
(51, 'uploads/portfolio/images/31_800x600.jpg', 10),
(52, 'uploads/portfolio/images/32_800x600.jpg', 10),
(53, 'uploads/portfolio/images/33_800x600.jpg', 10),
(54, 'uploads/portfolio/images/34_800x600.jpg', 10),
(55, 'uploads/portfolio/images/35_800x1067.jpg', 10),
(56, 'uploads/portfolio/images/36_800x640.jpg', 10),
(57, 'uploads/portfolio/images/37_800x602.jpg', 10),
(58, 'uploads/portfolio/images/38_800x602.jpg', 10),
(59, 'uploads/portfolio/images/39_800x602.jpg', 10),
(60, 'uploads/portfolio/images/40_800x602.jpg', 10),
(61, 'uploads/portfolio/images/41_800x602.jpg', 10),
(62, 'uploads/portfolio/images/42_800x602.jpg', 10),
(63, 'uploads/portfolio/images/43_800x602.jpg', 10),
(64, 'uploads/portfolio/images/44_800x602.jpg', 10),
(65, 'uploads/portfolio/images/45_800x602.jpg', 10),
(66, 'uploads/portfolio/images/46_800x602.jpg', 10),
(67, 'uploads/portfolio/images/47_800x602.jpg', 10),
(68, 'uploads/portfolio/images/1_800x1067.jpg', 11),
(69, 'uploads/portfolio/images/3_800x600.jpg', 11),
(70, 'uploads/portfolio/images/3_800x600_axIxMuh.jpg', 11),
(71, 'uploads/portfolio/images/4_800x600.jpg', 11),
(72, 'uploads/portfolio/images/5_800x600.jpg', 11),
(73, 'uploads/portfolio/images/6_800x600_J5UShCu.jpg', 11),
(74, 'uploads/portfolio/images/7_800x600_tdzsLwX.jpg', 11),
(75, 'uploads/portfolio/images/8_800x600_EjI9hiC.jpg', 11),
(76, 'uploads/portfolio/images/9_800x600_dmCGCnD.jpg', 11),
(77, 'uploads/portfolio/images/10_800x451.jpg', 11),
(78, 'uploads/portfolio/images/11_800x451.jpg', 11),
(79, 'uploads/portfolio/images/12_800x451.jpg', 11),
(80, 'uploads/portfolio/images/13_800x450.jpg', 11),
(81, 'uploads/portfolio/images/14_800x450.jpg', 11),
(82, 'uploads/portfolio/images/15_800x450.jpg', 11),
(83, 'uploads/portfolio/images/16_800x450.jpg', 11),
(84, 'uploads/portfolio/images/17_800x451.jpg', 11),
(85, 'uploads/portfolio/images/18_800x451.jpg', 11),
(86, 'uploads/portfolio/images/19_800x640.jpg', 11),
(87, 'uploads/portfolio/images/20_800x600.jpg', 11),
(88, 'uploads/portfolio/images/21_800x600.jpg', 11),
(89, 'uploads/portfolio/images/22_800x600.jpg', 11),
(90, 'uploads/portfolio/images/23_800x600_8KSnlTT.jpg', 11),
(91, 'uploads/portfolio/images/24_800x600_KjYQ9gQ.jpg', 11),
(92, 'uploads/portfolio/images/25_800x600_FlSfLHD.jpg', 11),
(93, 'uploads/portfolio/images/26_800x600_BZDHGid.jpg', 11),
(94, 'uploads/portfolio/images/1_800x1067_xI3QIbM.jpg', 12),
(95, 'uploads/portfolio/images/2_800x1067.jpg', 12),
(96, 'uploads/portfolio/images/3_800x1067_a04wIY3.jpg', 12),
(97, 'uploads/portfolio/images/4_800x1067.jpg', 12),
(98, 'uploads/portfolio/images/5_800x1067.jpg', 12),
(99, 'uploads/portfolio/images/6_800x1067.jpg', 12),
(100, 'uploads/portfolio/images/7_800x640.jpg', 12),
(101, 'uploads/portfolio/images/8_800x640.jpg', 12),
(102, 'uploads/portfolio/images/1_800x600_5ObzNZJ.jpg', 13),
(103, 'uploads/portfolio/images/2_800x601_boH62kF.jpg', 13),
(104, 'uploads/portfolio/images/3_800x653.jpg', 13),
(105, 'uploads/portfolio/images/4_800x868.jpg', 13),
(106, 'uploads/portfolio/images/5_800x1067_pnrhms2.jpg', 13),
(107, 'uploads/portfolio/images/6_800x1067_CbchVpw.jpg', 13),
(108, 'uploads/portfolio/images/7_800x600_UmWFJFP.jpg', 13),
(109, 'uploads/portfolio/images/8_800x484.jpg', 13),
(110, 'uploads/portfolio/images/9_800x600_cjJ7mwZ.jpg', 13),
(111, 'uploads/portfolio/images/10_800x600_RY3gkRH.jpg', 13),
(112, 'uploads/portfolio/images/11_800x600_86edkFB.jpg', 13),
(113, 'uploads/portfolio/images/12_800x1064.jpg', 13),
(114, 'uploads/portfolio/images/13_800x1064.jpg', 13),
(115, 'uploads/portfolio/images/14_800x600_5ewmxD9.jpg', 13),
(116, 'uploads/portfolio/images/1_800x1067_83EZbza.jpg', 14),
(117, 'uploads/portfolio/images/2_800x1067_XNAfGnl.jpg', 14),
(118, 'uploads/portfolio/images/3_800x1067_JHIVQZa.jpg', 14),
(119, 'uploads/portfolio/images/4_800x1067_H7hrnpJ.jpg', 14),
(120, 'uploads/portfolio/images/5_800x1067_pWr2NXV.jpg', 14),
(121, 'uploads/portfolio/images/6_800x1067_F9b5SpN.jpg', 14),
(122, 'uploads/portfolio/images/7_800x1067.jpg', 14),
(123, 'uploads/portfolio/images/8_800x1064.jpg', 14);

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_images`
--

INSERT INTO `project_images` (`id`, `title`, `image`, `project_id`) VALUES
(1, 'Лекарь', 'uploads/project/images/billboard_dj7boyE.jpg', 1),
(2, 'авыаыа', 'uploads/project/images/banner-slider_QcymhTC.jpg', 1),
(3, 'sadsad', 'uploads/project/images/news-page.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `title`, `company`, `text`) VALUES
(1, 'Надежда Викторовна', 'goog studio', 'Армада лучшая рекламно производственная компания в Бишкеке! Я сама в этом убедилась основываясь на их качество работы!'),
(2, 'Сергей Ковалюк', '30 шагов', 'Заказать рекламу? Только у РПК "Армада", мне нравится их подход к работе! Никогда не расстраивали и никаких нервов!');

-- --------------------------------------------------------

--
-- Table structure for table `services_category`
--

CREATE TABLE `services_category` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `cost` varchar(200) NOT NULL,
  `tdc` longtext NOT NULL,
  `text` longtext NOT NULL,
  `time` varchar(200) NOT NULL,
  `slug` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_category`
--

INSERT INTO `services_category` (`id`, `title`, `icon`, `cost`, `tdc`, `text`, `time`, `slug`) VALUES
(1, 'Объемные буквы', 'uploads/services/icons/3.png', 'от 55 сомов за 1 см', 'Стоимость и срок выполнения', '<p><strong>Наружная реклама</strong><span>&nbsp;</span>&mdash; графическая, текстовая, либо иная информация рекламного характера, которая размещается на специальных временных или стационарных конструкциях, расположенных на открытой местности, а также на внешних поверхностях зданий, сооружений, на элементах уличного оборудования, над проезжей частью улиц и дорог или на них самих, а также на автозаправочных станциях</p><p>Сегодня ни одна серьезная фирма, компания или организация, не сможет быть успешной на рынке товаров или услуг без грамотно проведенных промо акций. Благодаря такому рыночному инструменту, появляется возможность выделиться на фоне других конкурентов, и что не маловажно иметь постоянный спрос на свою продукцию.</p>', 'от 3 дней', 'obemnye-bukvy');

-- --------------------------------------------------------

--
-- Table structure for table `service_about`
--

CREATE TABLE `service_about` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL,
  `bold_text` longtext NOT NULL,
  `attach` varchar(100) NOT NULL,
  `attach_img` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_about`
--

INSERT INTO `service_about` (`id`, `title`, `slug`, `image`, `text`, `bold_text`, `attach`, `attach_img`) VALUES
(1, 'Наружная реклама', 'naruzhnaya-reklama', 'uploads/services/billboard_jqXyXLE.jpg', 'Наружная реклама — графическая, текстовая, либо иная информация рекламного характера, которая размещается на специальных временных или стационарных конструкциях, расположенных на открытой местности, а также на внешних поверхностях зданий, сооружений.', 'Виды наружной рекламы указаны ниже, выбрав вы можете ознакомится с ценами и со сроком изготовление рекламы', 'uploads/services/attach/3.png', 'uploads/services/attach_img/price-list_XNDzhDf.png');

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`id`, `title`, `image`) VALUES
(4, 'Форекс 3 мм', 'uploads/shop/forex1.jpg'),
(5, 'Акрил 3 мм более 30 цветов', 'uploads/shop/acril3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`id`, `title`, `text`) VALUES
(1, 'Создание сайта', '<p><strong><span>В наш спектр услуг входит:</span></strong></p><p><span>- Создание сайтов любой сложности</span><br><span>- Создание логотипов и фирменных стилей</span><br><span>- Создание брендбуков</span><br><span>- Создание Flash-презентаций</span><br><span>- Создание дизайна наружной рекламы</span><br><span>- Поддержка сайтов</span><br><span>- Продвижение и анализ сайтов</span><br><br><span>Стоимость сайтов:</span><br><strong>- Шаблонный сайт визитка с 4-мя страницами 200$- Срок выполнения 2 дня!!!</strong><span>&nbsp;Страницы: Главная, Услуги, Клиенты, Контакты. Модули: Слайдшоу, Обратная связь, Онлайн чат (для онлайн продаж услуг и товаров), Google maps.</span><br><span>- Landing page от 30 тыс сомов + привязка партнерской программы и 12 интернет кошельков.&nbsp;</span><br><span>- Сайт Визитка от 20 000 сомов</span><br><span>- Сайт Портал от 50 000 сомов</span><br><span>- Сайт Портфолио от 30 000 сомов</span><br><span>- Сайт Блог от 25 000 сомов</span><br><span>- Сайт Социальной сети от 100 000 сомов</span><br><span>- Интернет-магазин 60 000 сомов</span><br><br><span>Так же Вы можете приобрести пакеты на создание сайтов:</span><br><span>- Базовый = 600$</span><br><span>- Оптимальный = 1000$</span><br><span>- Уникальный = 3000$</span><br><span>- PRO = 5000$&nbsp;</span></p><p><br></p><p><strong>Наши контакты:</strong></p><p><a target="_blank" href="http://googstudio.com/">googstudio.com</a><br><span>e-mail: <a href="mailto:googmstudio@gmail.com">googmstudio@gmail.com</a></span><br><span>skype: goog.studio</span><br><span>тел: +996 557 880 182</span><br><span>+996 312 680 969</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `icon`, `text`) VALUES
(1, 'Дизайн интерьера', 'uploads/slider/image/1_013hSOi.png', 'uploads/slider/icon/1_ZHdcutW.png', 'Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Donec sollicitudin molestie malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Proin eget tortor risus.');

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `title`, `image`, `url`) VALUES
(1, 'Facebook', 'uploads/armada/social/1_0zvVhwa.png', 'https://www.facebook.com/rpkarmada');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`),
  ADD KEY `about_2dbcba41` (`slug`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `armada_footerinfo`
--
ALTER TABLE `armada_footerinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_slug_e924580b_uniq` (`slug`),
  ADD KEY `blog_2dbcba41` (`slug`);

--
-- Indexes for table `call`
--
ALTER TABLE `call`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_de54fa62` (`expire_date`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Globalmeta`
--
ALTER TABLE `Globalmeta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `law`
--
ALTER TABLE `law`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Localmeta`
--
ALTER TABLE `Localmeta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Localmeta_64458f32` (`blog_id`),
  ADD KEY `Localmeta_c660c5da` (`news_id`),
  ADD KEY `Localmeta_b0dc1e29` (`service_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_slug_0412a712_uniq` (`slug`),
  ADD KEY `news_2dbcba41` (`slug`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_images`
--
ALTER TABLE `portfolio_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_images_b098ad43` (`project_id`);

--
-- Indexes for table `project_images`
--
ALTER TABLE `project_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_images_b098ad43` (`project_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_category`
--
ALTER TABLE `services_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_category_slug_82e59540_uniq` (`slug`),
  ADD KEY `services_category_2dbcba41` (`slug`);

--
-- Indexes for table `service_about`
--
ALTER TABLE `service_about`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_about_2dbcba41` (`slug`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `armada_footerinfo`
--
ALTER TABLE `armada_footerinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `call`
--
ALTER TABLE `call`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Globalmeta`
--
ALTER TABLE `Globalmeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `law`
--
ALTER TABLE `law`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Localmeta`
--
ALTER TABLE `Localmeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `portfolio_images`
--
ALTER TABLE `portfolio_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `project_images`
--
ALTER TABLE `project_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `services_category`
--
ALTER TABLE `services_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `service_about`
--
ALTER TABLE `service_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `Localmeta`
--
ALTER TABLE `Localmeta`
  ADD CONSTRAINT `Localmeta_blog_id_edb39718_fk_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`),
  ADD CONSTRAINT `Localmeta_news_id_edde0d96_fk_news_id` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`),
  ADD CONSTRAINT `Localmeta_service_id_61e814f2_fk_about_id` FOREIGN KEY (`service_id`) REFERENCES `about` (`id`);

--
-- Constraints for table `portfolio_images`
--
ALTER TABLE `portfolio_images`
  ADD CONSTRAINT `portfolio_images_project_id_8fdd53a4_fk_portfolio_id` FOREIGN KEY (`project_id`) REFERENCES `portfolio` (`id`);

--
-- Constraints for table `project_images`
--
ALTER TABLE `project_images`
  ADD CONSTRAINT `project_images_project_id_a70d31ae_fk_services_category_id` FOREIGN KEY (`project_id`) REFERENCES `services_category` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
