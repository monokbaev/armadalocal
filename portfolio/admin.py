from django.contrib import admin
from .models import Image, Portfolio
from armadaLocal.utils import thumbnail
# Register your models here.
class ImageInline(admin.StackedInline):
    model = Image
    extra = 1

class AdminPorfolio(admin.ModelAdmin):
    list_display = ('title',)
    inlines = [ImageInline,]


admin.site.register(Portfolio, AdminPorfolio)
