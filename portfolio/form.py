from django import forms

from portfolio.models import Image

class PortfolioForm(forms.Form):

    class Meta:
        model = Image