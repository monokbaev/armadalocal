from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
import random, os

# Create your models here.
def make_upload_path(instance, filename, prefix = False):
    name, ext = 'image', os.path.splitext(filename)[1]
    n1 = random.randint(0, 1000)
    n2 = random.randint(0, 1000)
    n3 = random.randint(0, 1000)
    filename = str(n1)+'_'+str(n2)+'_'+str(n3)+'.'+ext
    return '%s/%s' %('uploads/portfolio/images', filename)

class Portfolio(models.Model):
    title = models.CharField( max_length = 100, verbose_name = _('Title'))


    class Meta:
        db_table = 'portfolio'
        verbose_name = _('Portfolio')
        verbose_name_plural = _('Portfolio')

    def __unicode__(self):
        return self.title

class Image(models.Model):
    image = models.ImageField(upload_to = 'uploads/portfolio/images', blank = True, verbose_name = _('Image'))
    project = models.ForeignKey(Portfolio, blank = True, related_name = 'project2', verbose_name = _('Project'))
    def pic(self):
        if self.image:
            return '<img src="%s" width="70">' %self.image.url
        else:
            return '(none)'


    class Meta:
        db_table = 'portfolio_images'
        verbose_name = _('Image')
        verbose_name_plural = _('Project Images')