from django.shortcuts import render
from django.template.context_processors import csrf

from .models import *
from armada.models import GlobalMeta, Social, Offer, Address, Contact, FooterInfo
from django.views import generic
# Create your views here.
def index(request, context = {}):
    context.update(csrf(request))
    try:
        portfolio = Portfolio.objects.all()
    except Portfolio.DoesNotExist:
        portfolio = False
    context ['portfolio'] = portfolio
    #
    # context['images']={}
    # for port in portfolio:
    #     context['images'][port.id] = Image.objects.filter(project=port)

    try:
        images = Image.objects.all()
    except Image.DoesNotExist:
        images = False
    context ['images'] = images
    context['lmeta'] = GlobalMeta.objects.filter(title="Portfolio")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    context['socials'] = Social.objects.all()
    context['offers'] = Offer.objects.all()
    context['addresses'] = Address.objects.all()
    context['contacts'] = Contact.objects.all()
    context['finfo'] = FooterInfo.objects.get()

    return render(request, 'portfolio/index.html', context)

    # class DetailView(generic.DetailView):
    #     model = Portfolio
    #     template_name = 'portfolio/detail.html'
    #     context_object_name = 'portfolio'
    #
    #     def get_context_data(self, *args, **kwargs):
    #         context = super(DetailView, self).get_context_data(**kwargs)
    #         context['images'] = Image.objects.all()
    #
    #         return context