from __future__ import unicode_literals

from django.db import models
from froala_editor.fields import FroalaField

# Create your models here.


class Article(models.Model):
    title = models.CharField(max_length= 255)
    image = models.ImageField(upload_to='uploads/shop', default=None)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'shop'
        verbose_name = ('Shop')
        verbose_name_plural = ('Shops')