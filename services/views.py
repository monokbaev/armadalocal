from django.shortcuts import render, render_to_response
from django.http import Http404
from django.template.context_processors import csrf

from .models import About, Category, Image
from django.views import generic
from armada.models import Contact, Address, FooterInfo, GlobalMeta, LocalMeta, Offer, Social
# Create your views here.


def service_detail_view(request, context={}, slug=""):
    context.update(csrf(request))
    context['images'] = Image.objects.all()
    context['contacts'] = Contact.objects.all()
    context['addresses'] = Address.objects.all()
    context['finfo'] = FooterInfo.objects.get()
    context['socials'] = Social.objects.all()
    context['offers'] = Offer.objects.all()
    context['lmeta'] = LocalMeta.objects.filter(service__slug=slug)
    try:
        cats = Category.objects.get(slug=slug)
    except Category.DoesNotExist:
        cats = False
    context['cats'] = cats
    return render_to_response('services/detail.html', context)

def index(request, context = {}):
    context.update(csrf(request))
    try:
        abouts = About.objects.all()
    except About.DoesNotExist:
        abouts = False
    context ['abouts'] = abouts
    try:
        finfo = FooterInfo.objects.get()
    except FooterInfo.DoesNotExist:
        finfo = False
    context ['finfo'] = finfo
    try:
        address = Address.objects.all()
    except Address.DoesNotExist:
        address = False
    context['addresses'] = address
    try:
        contact = Contact.objects.all()
    except Contact.DoesNotExist:
        contact = False
    context['contacts'] = contact

    try:
        cats = Category.objects.all()
    except Category.DoesNotExist:
        cats = False
    context ['cats'] = cats
    context['lmeta'] = GlobalMeta.objects.filter(title="Service")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    context['offers'] = Offer.objects.all()
    context['socials'] = Social.objects.all()


    return render(request, 'services/index.html', context)



# class DetailView(generic.DetailView):
#     model = Category
#     template_name = 'services/detail.html'
#     context_object_name = 'cats'
#
#     def get_context_data(self, *args, **kwargs):
#         context = super(DetailView, self).get_context_data(**kwargs)
#         context['images'] = Image.objects.all()
#         context['contacts'] = Contact.objects.all()
#         context['addresses'] = Address.objects.all()
#         context['finfo'] = FooterInfo.objects.get()
#         context['lmeta'] = LocalMeta.objects.filter(service__slug=slug)
#
#         return context

