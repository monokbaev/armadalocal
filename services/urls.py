# coding: utf8

from django.conf.urls import url
from . import views
app_name = 'services'

urlpatterns = [
    url(r'^$', views.index , name = 'index_services'),
    url(r'^(?P<slug>[-_\w]+)/$', "services.views.service_detail_view", name='detail'),
]
