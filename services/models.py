# coding:utf8
from __future__ import unicode_literals
import random, os
from django.utils.translation import ugettext_lazy as _
from django.db import models
from froala_editor.fields import FroalaField



# Create your models here.

def make_upload_path(instance, filename, prefix = False):
    name, ext = 'image', os.path.splitext(filename)[1]
    n1 = random.randint(0, 1000)
    n2 = random.randint(0, 1000)
    n3 = random.randint(0, 1000)
    filename = str(n1)+'_'+str(n2)+'_'+str(n3)+'.'+ext
    return '%s/%s' %('uploads/project/images', filename)

class About(models.Model):
    title = models.CharField(max_length = 50, verbose_name = _('Title'))
    slug = models.SlugField(max_length = 55, verbose_name = _('Slug'))
    image = models.ImageField(upload_to = 'uploads/services')
    text = models.TextField()
    bold_text = models.TextField()
    attach = models.FileField(upload_to = 'uploads/services/attach')
    attach_img = models.ImageField(upload_to = 'uploads/services/attach_img', default= False)


    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'service_about'
        verbose_name = _('About')
        verbose_name_plural = _('About_servise')

class Category(models.Model):
    title = models.CharField(max_length = 100, verbose_name = _('Title'))
    slug = models.SlugField(max_length = 55, verbose_name = _('Slug'), unique=True)
    icon = models.ImageField(upload_to = 'uploads/services/icons')
    text = FroalaField(verbose_name = _('Text'))
    tdc = models.TextField(verbose_name = _('Title_tdc'))
    time = models.CharField(max_length = 200, verbose_name = _('Time'))
    cost = models.CharField(max_length = 200, verbose_name = _('Cost'))


    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Cat_servise')

class Image(models.Model):
    title = models.CharField(max_length = 255, blank = True, verbose_name = _('Title'))
    image = models.ImageField(upload_to = 'uploads/project/images', blank = True, verbose_name = _('Image'))
    project = models.ForeignKey(Category, blank = True, related_name = 'project1', verbose_name = _('Project'))

    def pic(self):
        if self.image:
            return '<img src="%s" width="70">' %self.image.url
        else:
            return '(none)'
    pic.short_description = _('Big Image')
    pic.allow_tags = True

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'project_images'
        verbose_name = _('Image')
        verbose_name_plural = _('Project Images')