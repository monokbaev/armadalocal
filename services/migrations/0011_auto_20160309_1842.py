# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-09 12:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0010_auto_20160309_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(max_length=55, unique=True, verbose_name='Slug'),
        ),
    ]
