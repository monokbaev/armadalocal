from django import forms

from services.models import Image

class ImageForm(forms.Form):

    class Meta:
        model = Image