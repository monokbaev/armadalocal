from django.contrib import admin
from armadaLocal.utils import thumbnail
from .models import *
# Register your models here.

class AboutAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'slug')


class ImageInline(admin.StackedInline):
    model = Image
    extra = 1

class AdminCategory(admin.ModelAdmin):
    list_display = ('title', 'image_url')
    prepopulated_fields = {"slug": ("title",)}
    inlines = [ImageInline,]

    def image_url(self, project):
        image_path = thumbnail(project.icon, '50x50')
        return '<a href="'+ str(project.id) +'/"><img src="'+ image_path +'"/></a>'

    image_url.short_description = 'Thumbnail'
    image_url.allow_tags = True

admin.site.register(About, AboutAdmin)
admin.site.register(Category,AdminCategory)