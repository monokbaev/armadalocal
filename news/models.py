# coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from froala_editor.fields import FroalaField

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length = 100, verbose_name = _('Title'))
    slug = models.SlugField(max_length = 55, verbose_name = _('Slug'), blank = True, unique=True)
    image = models.ImageField(upload_to= 'uploads/news/images',verbose_name =_('Image'), default = False)
    text = FroalaField(verbose_name = _('Text'))
    shown = models.IntegerField(default = 0, verbose_name = _('Views'), editable = False)
    created = models.DateTimeField(auto_now_add = True, verbose_name = _('Publication date'))


    class Meta:
        db_table = 'news'
        verbose_name = _('News')
        verbose_name_plural = _('News')

    def __unicode__(self):
        return self.title