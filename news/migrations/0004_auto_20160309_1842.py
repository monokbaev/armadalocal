# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-09 12:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_auto_20160308_2322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=models.SlugField(blank=True, max_length=55, unique=True, verbose_name='Slug'),
        ),
    ]
