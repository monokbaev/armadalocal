# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-01 05:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backcall', '0004_call_service'),
    ]

    operations = [
        migrations.AlterField(
            model_name='call',
            name='service',
            field=models.CharField(default='\u0423\u0441\u043b\u0443\u0433\u0430 \u043d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\u0430', max_length=50, verbose_name='\u0423\u0441\u043b\u0443\u0433\u0430'),
        ),
    ]
