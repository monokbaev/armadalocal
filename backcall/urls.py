# coding: utf8

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^send$', views.sendAjax, name="send_ajax"),
    url(r'^call/$', views.sendAjax2, name="sendajax"),
]