# coding: utf8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models


# Create your models here.
class Call(models.Model):
    name = models.CharField(verbose_name = _('Name'), max_length = 255)
    phone = models.CharField(verbose_name=_('Phone number'), max_length = 50, null=True)
    service = models.CharField(verbose_name="Услуга", max_length=50, default="Услуга не выбрана")

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'call'
        verbose_name = _('Call')
        verbose_name_plural = _('Calls')


class Feedback(models.Model):
    name = models.CharField(verbose_name = _('Name'), max_length = 255)
    email = models.EmailField(verbose_name=_('Phone number'), max_length = 255)
    text = models.TextField()

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'feedback'
        verbose_name = _('Feedback')
        verbose_name_plural = _('Feedbacks')