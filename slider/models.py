from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.db import models

# Create your models here.


class Slider(models.Model):
    title = models.CharField(max_length = 255, verbose_name = _('Title'))
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/slider/image')
    icon = models.ImageField(verbose_name = _('Icon'), upload_to = 'uploads/slider/icon')
    text = models.TextField(verbose_name = _('Text'))

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'slider'
        verbose_name = _('Slider')
        verbose_name_plural = _('Sliders')