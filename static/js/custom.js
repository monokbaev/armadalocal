

$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

(function($) {
  $(document).ready(function() {

  	var $portfolioContainer = $('.tab-content ul').width();
    var $portfolioItem = $('.tab-content ul li');
			if($portfolioItem.length>0) {

     		var $per_line = 3;
        if($(window).width() < 480) {
        	$per_line = 2;
        } else if ($(window).width() < 768) {
        	$per_line = 2;
        }
        $portfolioItem.each(function() {
          var $size = Math.floor($portfolioContainer/$per_line);
					$(this).css({'width':($size)+'px','height':($size)+'px'});
					$(this).css({'float':'left'});
					console.log($size);
        });

  	};

  });
})(jQuery);
