from django.shortcuts import render
from django.template.context_processors import csrf

from .models import *
from armada.models import Contact, Address, FooterInfo, LocalMeta, GlobalMeta, Social, Offer
# Create your views here.


def index(request, context = {}):
    context.update(csrf(request))
    try:
        article = Article.objects.all()
    except Article.DoesNotExist:
        article = False
    context ['article'] = article
    try:
        finfo = FooterInfo.objects.get()
    except FooterInfo.DoesNotExist:
        finfo = False
    context ['finfo'] = finfo
    try:
        address = Address.objects.all()
    except Address.DoesNotExist:
        address = False
    context['addresses'] = address
    try:
        contact = Contact.objects.all()
    except Contact.DoesNotExist:
        contact = False
    context['contacts'] = contact
    context['lmeta'] = GlobalMeta.objects.filter(title="Law")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    context['socials'] = Social.objects.all()
    context['offers'] = Offer.objects.all()

    return render(request, 'law/index.html', context)