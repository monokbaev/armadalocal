# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-16 13:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('armada', '0009_footerinfo_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='footerinfo',
            name='title',
            field=models.CharField(default='', max_length=250, verbose_name='Title'),
        ),
    ]
