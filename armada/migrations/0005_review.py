# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-16 08:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('armada', '0004_auto_20160116_0755'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('company', models.CharField(max_length=255, verbose_name='Company')),
                ('text', models.TextField()),
            ],
            options={
                'db_table': 'review',
                'verbose_name': 'Review',
                'verbose_name_plural': 'Reviews',
            },
        ),
    ]
