# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-09 09:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('armada', '0020_auto_20160309_1526'),
    ]

    operations = [
        migrations.RenameField(
            model_name='localmeta',
            old_name='blog',
            new_name='local',
        ),
    ]
