# coding: utf8

from django.conf.urls import url
from . import views


app_name = 'armada'

urlpatterns = [
    url(r'^$', views.index, name = 'index'),

    url(r'^news/(?P<slug>[-_\w]+)/$', 'armada.views.news_detail_view', name='news_detail'),

    url(r'^blog/(?P<slug>[-_\w]+)/$', 'armada.views.blog_detail_view', name='blogs_detail'),
    url(r'^news/', 'armada.views.news_list_view', name='news_list'),
    url(r'^blog/', 'armada.views.blog_list_view', name='blogs_list'),
    url(r'^contact/', views.contact, name='contact'),



]



