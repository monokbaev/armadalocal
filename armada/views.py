#coding: utf8


from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, render_to_response
from django.template.context_processors import csrf
from django.views import generic
# local
from client.models import Client
from news.models import News
from blog.models import Blog
from armada.models import *
from slider.models import Slider
from django.shortcuts import render, redirect, HttpResponse, get_object_or_404

# Create your views here.


def blog_list_view(request, context={}):
    context.update(csrf(request))
    blog_list = Blog.objects.all().order_by('-created')
    context['contacts'] = Contact.objects.all()
    context['addresses'] = Address.objects.all()
    context['finfo'] = FooterInfo.objects.get()
    context['lmeta'] = GlobalMeta.objects.filter(title="Blog")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    paginator = Paginator(blog_list, 4)
    page = request.GET.get('page')

    try:
        blog = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        blog = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        blog = paginator.page(paginator.num_pages)

    context['blog'] = blog
    return render_to_response('blogs/list.html', context)

def blog_detail_view(request, context={}, slug=""):
    context.update(csrf(request))
    context['contacts'] = Contact.objects.all()
    context['addresses'] = Address.objects.all()
    context['finfo'] = FooterInfo.objects.get()
    context['gmeta'] = GlobalMeta.objects.all()
    context['lmeta'] = LocalMeta.objects.filter(blog__slug=slug)
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    context['blog'] = Blog.objects.get(slug=slug)
    return render_to_response('blogs/detail.html', context)


def news_detail_view(request, context={}, slug=""):
    context.update(csrf(request))
    context['news'] = News.objects.get(slug=slug)
    context['contacts'] = Contact.objects.all()
    context['addresses'] = Address.objects.all()
    context['finfo'] = FooterInfo.objects.get()
    context['lmeta'] = LocalMeta.objects.filter(news__slug=slug)
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    context['socials'] = Social.objects.all()
    return render_to_response("news/detail.html", context)


def news_list_view(request, context={}):
    context.update(csrf(request))
    news_list = News.objects.all().order_by('-created')
    context['contacts'] = Contact.objects.all()
    context['addresses'] = Address.objects.all()
    context['finfo'] = FooterInfo.objects.get()
    context['socials'] = Social.objects.all()
    context['lmeta'] = GlobalMeta.objects.filter(title="News")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    paginator = Paginator(news_list, 4)
    page = request.GET.get('page')

    try:
        news = paginator.page(page)
    except PageNotAnInteger:
    # If page is not an integer, deliver first page.
        news = paginator.page(1)
    except EmptyPage:
    # If page is out of range (e.g. 9999), deliver last page of results.
        news = paginator.page(paginator.num_pages)

    context['news'] = news
    return render_to_response('news/list.html', context)


def index(request, context = {}):

    try:
        clients = Client.objects.all()
    except Client.DoesNotExist:
        clients = False
    context['clients'] = clients
    try:
        news = News.objects.all().order_by('-created')[:2]
    except News.DoesNotExist:
        news = False
    context['news'] = news
    try:
        blog = Blog.objects.all().order_by('-created')[:2]
    except Blog.DoesNotExist:
        blog = False
    context['blogs'] = blog
    try:
        contact = Contact.objects.all()
    except Contact.DoesNotExist:
        contact = False
    context['contacts'] = contact
    try:
        social = Social.objects.all()
    except Social.DoesNotExist:
        social = False
    context['socials'] = social
    try:
        address = Address.objects.all()
    except Address.DoesNotExist:
        address = False
    context['addresses'] = address
    try:
        about = About.objects.get(slug = 'o-nas')
    except About.DoesNotExist:
        about = False
    context['about'] = about
    try:
        answer = About.objects.get(slug = 'pochemu-my')
    except About.DoesNotExist:
        answer = False
    context['answer'] = answer
    try:
        slider = Slider.objects.all()
    except Slider.DoesNotExist:
        slider = False
    context['sliders'] = slider
    try:
        review = Review.objects.all()
    except Review.DoesNotExist:
        review = False
    context['reviews'] = review
    context['title'] = 'Main'
    try:
        offer = Offer.objects.all()
    except Offer.DoesNotExist:
        offer = False
    context['offers'] = offer
    try:
        finfo = FooterInfo.objects.get()
    except FooterInfo.DoesNotExist:
        finfo = False
    context ['finfo'] = finfo
    try:
       context['gmeta'] = GlobalMeta.objects.filter(title="Global")
    except GlobalMeta.DoesNotExist:
       globalmeta = False

    return render(request, 'armada/index.html', context)


# class NewsListView(generic.ListView):
#     model = News
#     template_name = 'news/list.html'
#     context_object_name = 'news'
#
#     def get_context_data(self, *args, **kwargs):
#         context = super(NewsListView, self).get_context_data(**kwargs)
#         news_list = News.objects.all().order_by('-created')
#         context['contacts'] = Contact.objects.all()
#         context['addresses'] = Address.objects.all()
#         context['finfo'] = FooterInfo.objects.get()
#         context['lmeta'] = GlobalMeta.objects.filter(title="News")
#         context['gmeta'] = GlobalMeta.objects.filter(title="Global")
#         paginator = Paginator(news_list, 1)
#         page = request.GET.get('page')
#
#         try:
#             news = paginator.page(page)
#         except PageNotAnInteger:
#         # If page is not an integer, deliver first page.
#             news = paginator.page(1)
#         except EmptyPage:
#         # If page is out of range (e.g. 9999), deliver last page of results.
#             news = paginator.page(paginator.num_pages)
#
#         context['news'] = news
#         return context

# class NewsDetailView(generic.DetailView):
#     model = News
#     template_name = 'news/detail.html'
#     context_object_name = 'news'
#
#     def get_context_data(self, *args, **kwargs):
#         context = super(NewsDetailView, self).get_context_data(**kwargs)
#         context['news'] = News.objects.get(slug = kwargs['object'].slug)
#         context['contacts'] = Contact.objects.all()
#         context['addresses'] = Address.objects.all()
#         context['finfo'] = FooterInfo.objects.get()
#
#         return context


# class BlogListView(generic.ListView):
#     model = Blog
#     template_name = 'blogs/list.html'
#     context_object_name = 'blogs'
#
#     def get_context_data(self, *args, **kwargs):
#         context = {}
#         context['blogs'] = Blog.objects.all().order_by('-created')
#         context['contacts'] = Contact.objects.all()
#         context['addresses'] = Address.objects.all()
#         context['finfo'] = FooterInfo.objects.get()
#         context['lmeta'] = GlobalMeta.objects.filter(title="Blog")
#         context['gmeta'] = GlobalMeta.objects.filter(title="Global")
#
#         return context


# class BlogDetailView(generic.DetailView):
#     model = Blog
#     template_name = 'blogs/detail.html'
#     context_object_name = 'blog'
#
#     def get_object(self, queryset=None):
#         return get_object_or_404(Blog, slug = self.kwargs.get('slug'))
#         context['contacts'] = Contact.objects.all()
#         context['addresses'] = Address.objects.all()
#         context['finfo'] = FooterInfo.objects.get()
#         context['gmeta'] = GlobalMeta.objects.get()
#         context['lmeta'] = LocalMeta.objects.get()
#
#
#         return context

def contact(request, context = {}):
    try:
        contact = Contact.objects.all()
    except Contact.DoesNotExist:
        contact = False
    context ['contacts'] = contact
    context['contacts'] = Contact.objects.all()
    context['offers'] = Offer.objects.all()
    context['socials'] = Social.objects.all()
    context['addresses'] = Address.objects.all()
    context['finfo'] = FooterInfo.objects.get()
    context['lmeta'] = GlobalMeta.objects.filter(title="Contact")
    context['gmeta'] = GlobalMeta.objects.filter(title="Global")

    return render(request, 'contact.html', context)


def globalmeta(request, context={}):
   try:
       globalmeta = GlobalMeta.objects.get(pk=1)
   except GlobalMeta.DoesNotExist:
       globalmeta = False
   context['globalmeta'] = globalmeta



