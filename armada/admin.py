from django.contrib import admin
from .models import *
# Register your models here.


class AboutAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ('title',)}
    list_display = ('title', 'slug',)

admin.site.register(Social)
admin.site.register(Contact)
admin.site.register(Address)
admin.site.register(Review)
admin.site.register(About, AboutAdmin)
admin.site.register(Offer)
admin.site.register(FooterInfo)
admin.site.register(GlobalMeta)
admin.site.register(LocalMeta)