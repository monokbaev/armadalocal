# coding: utf8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from froala_editor.fields import FroalaField
from django.db import models
from blog.models import Blog
from news.models import News
from law.models import Article
from services.models import About

# Create your models here.

class About(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    slug = models.SlugField(max_length = 55, verbose_name = _('Url'))
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/armada/about')
    text = FroalaField(verbose_name = _('Text'))

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'about'
        verbose_name = _('About')
        verbose_name_plural = _('About')

class Social(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/armada/social')
    url = models.CharField(verbose_name = _('Url'), max_length = 255)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'social'
        verbose_name = _('Social')
        verbose_name_plural = _('Socials')

class Contact(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    image = models.ImageField(verbose_name = _('Image'), blank = True, null = True, upload_to = 'uploads/armada/contact')
    phone = models.CharField(verbose_name = _('Phone number'),max_length = 255, default = 0)


    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'contact'
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

class Address(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    address = models.CharField(verbose_name = _('Address'),max_length = 255, default = 0)


    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'address'
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

class Review(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    company = models.CharField(verbose_name = _('Company'), max_length = 255)
    text = models.TextField()

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'review'
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')

class Offer(models.Model):
    title = models.CharField(verbose_name= _('Title'), max_length = 255)
    image = models.ImageField(verbose_name = _('Image'), blank = True, null = True, upload_to = 'uploads/armada/offer')
    text = models.TextField(verbose_name = _('Text'))

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'offer'
        verbose_name = _('Offer')
        verbose_name_plural = _('Offers')


class FooterInfo(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 250, default = '')
    image = models.ImageField(verbose_name = _('Logo'), upload_to = 'uploads/armada/footer')
    text = FroalaField(verbose_name = _('Text'))
    created = models.CharField(verbose_name = _('Created Date'), max_length = 250)
    created_by = models.CharField(verbose_name = _('Created By'), max_length = 100)
    logo = models.ImageField(verbose_name = _('Goog Studio logo'), upload_to = 'uploads/armada/goog_logo')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('footer info')
        verbose_name_plural = _('Footer Info')


class GlobalMeta(models.Model):
    title = models.CharField(max_length=55, default=None, help_text="Для указания локальной мета-информации на странице всех новостей укажите здесь 'News', для страницы всех блогов 'Blog', для страницы контактов 'Contacts' ")
    meta_title = models.CharField(max_length = 100, verbose_name = _('Мета header'), blank = True)
    meta_desc = models.TextField(verbose_name = _('Мета description'), blank = True)
    meta_key = models.CharField(max_length = 200, verbose_name = ('Мета keywords'), blank = True)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'Globalmeta'
        verbose_name = _('Meta')
        verbose_name_plural = _('Metas')


class LocalMeta(models.Model):
    meta_title = models.CharField(max_length = 100, verbose_name = _('Мета header'), blank = True)
    meta_desc = models.TextField(verbose_name = _('Мета description'), blank = True)
    meta_key = models.CharField(max_length = 200, verbose_name = ('Мета keywords'), blank = True)
    blog = models.ForeignKey(Blog, related_name="blog", blank=True, null=True)
    news = models.ForeignKey(News, related_name="news", blank=True, null=True)
    service = models.ForeignKey(About, related_name='service', blank=True, null=True)

    def __unicode__(self):
        return self.meta_title

    class Meta:
        db_table = 'Localmeta'
        verbose_name = _('lMeta')
        verbose_name_plural = _('lMetas')